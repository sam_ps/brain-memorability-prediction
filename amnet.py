import torch
from torchvision import transforms
import cv2
from lamem2 import  *
from amnet_model import *
import amnet_model as amnet_model
from config import *

class PredictionResult():
    def __init__(self):
        self.rc = 0
        self.mse  = 0
        self.predictions = []
        self.targets = []
        self.outputs = []
        self.attention_masks = []
        self.inference_took = 0
        self.image_names = []
        self.images = None # If source images are pre-loaded this atribute will hold their bitmaps

    def write_to_dict(self):
        results = dict()
        for _, (image, prediction) in enumerate(zip(self.image_names, self.predictions)):
            results[str(image)] = prediction
        return results

    def get_attention_maps(self, show=False):

        images = self.image_names
        att_maps = self.attention_masks

        num_images = len(att_maps)
        seq_len = self.outputs.shape[1]
        ares = int(np.sqrt(att_maps.shape[2]))

        amaps_imgs = []
        out_size = (224, 224)

        for b in range(num_images):

            # Read the source image and resize it to 224x224
            if self.images is None:
                img = cv2.imread(images[b])
            else:
                img = self.images[b]

            img = cv2.resize(img, out_size)

            # Create an empty output image
            offset = 20
            canvas = np.zeros((224+offset*2+50, (224+offset*2)*(seq_len+1), 3), dtype=np.uint8)
            canvas[offset:224+offset, offset:224+offset,:] = img

            amaps = []

            # Get min/max pixel values across all attention maps
            att_max = 0
            att_min = 9999999
            local_norm = True
            for s in range(seq_len):
                img_alpha = att_maps[b,s]
                img_alpha = img_alpha.reshape((ares, ares))
                Min = img_alpha.min()
                Max = img_alpha.max()
                if att_max < Max: att_max = Max
                if att_min > Min: att_min = Min

            for s in range(seq_len):
                img_alpha = att_maps[b,s]
                img_alpha = img_alpha.reshape((ares, ares))

                # Normalize & convert to uint8
                if local_norm:
                    Min = img_alpha.min()
                    img_alpha -= Min
                    Max = img_alpha.max()
                    if (Max != 0):
                        img_alpha = img_alpha/Max
                else:
                    img_alpha_min = img_alpha - img_alpha.min()
                    if (att_max-att_min) > 0:
                        img_alpha_min = img_alpha_min / (att_max-att_min)
                    else:
                        print("Zero diff in alpha map!")
                    img_alpha = img_alpha_min - (img_alpha.min()-att_min)


                img_alpha = img_alpha * 255
                img_alpha = img_alpha.astype(np.uint8)

                # Scale to the source image dimensions
                heat_map_img = cv2.resize(img_alpha, out_size, interpolation=cv2.INTER_CUBIC)
                heat_map_img = cv2.applyColorMap(heat_map_img, cv2.COLORMAP_JET)

                alpha = 0.5
                beta = (1.0 - alpha)
                img_heat_map_blend = cv2.addWeighted(img, alpha, heat_map_img, beta, 0.0)
                # amaps.append(img_heat_map_blend)

                y_pos= (s+1) * (224+offset*2)
                canvas[offset:224 + offset, y_pos+offset:y_pos+224+offset, :] = img_heat_map_blend

            amaps.append(canvas)

            if show:
                cv2.imshow('Images with attention maps', canvas)
                cv2.waitKey(0)

            amaps_imgs.append(amaps)

        return amaps_imgs


    def write_attention_maps(self, out_dir):
        os.makedirs(out_dir, exist_ok=True)
        att_maps = self.get_attention_maps()

        for att_map, source_image_filename in zip(att_maps, self.image_names):
            path, filename = os.path.split(source_image_filename)
            out_filename, ext = os.path.splitext(filename)
            out_filename += '_att'+ext
            out_filename = os.path.join(out_dir, out_filename)
            cv2.imwrite(out_filename, att_map[0])

        return

class AMNet:

    def __init__(self):
        self.model = None
        self.test_transform = None
        return

    def init(self, hps):
        self.hps = hps

        core_cnn = getattr(amnet_model, hps.front_end_cnn)()
        model = AMemNetModel(core_cnn, hps, a_res=14, a_vec_size=1024)

        rnd_seed = 42
        np.random.seed(rnd_seed)
        torch.manual_seed(rnd_seed)

        if hps.use_cuda:
            torch.cuda.set_device(hps.cuda_device)
            if settings.VERBOSE:
                print("Current CUDA device: ", torch.cuda.current_device())
            torch.cuda.manual_seed(rnd_seed)

        self.model = model
        self.init_transformations()
        self.load_checkpoint(self.hps.model_weights)
        return

    def init_transformations(self):

        if self.hps.torchvision_version_major == 0 and self.hps.torchvision_version_minor < 2:
            _resize = transforms.Scale
            _rnd_resize_crop = transforms.RandomSizedCrop
        else:
            _resize = transforms.Resize
            _rnd_resize_crop = transforms.RandomResizedCrop

        # Test
        self.test_transform = transforms.Compose([
            _resize([224, 224]),
            transforms.ToTensor(),
            transforms.Normalize(mean=self.hps.img_mean, std=self.hps.img_std)
        ])

        return

    def load_checkpoint(self, filename):

        if filename.strip() == '':
            return False

        try:
            if settings.VERBOSE:
                print('Loading checkpoint: ', filename)
            cpnt = torch.load(filename, map_location=lambda storage, loc: storage)
            self.experiment_path, filename = os.path.split(filename)
        except FileNotFoundError:
            if settings.VERBOSE:
                print("Cannot open file: ", filename)
            self.model_weights_current = ''
            return False

        try:
            self.model.load_weights(cpnt['model'])
        except:
            self.model.load_state_dict(cpnt['model'])

        return True


    def postprocess(self, output, outputs):

        if self.hps.last_step_prediction:
            output = outputs[:,-1:]
        else:
            output = (outputs).sum(1)
            output = output / outputs.shape[1]

        output /= self.hps.target_scale
        output = output + self.hps.target_mean

        if self.hps.last_step_prediction:
            outputs[:] = 0
            outputs[:,-1:] = output
        else:
            outputs = (outputs / (outputs.shape[1] * self.hps.target_scale)) + self.hps.target_mean / outputs.shape[1]

        return output, outputs


    def predict(self, test_loader):
        self.model.eval()
        if self.hps.use_cuda:
            self.model.cuda()

        pr = PredictionResult()

        predictions = []
        targets = []
        output = None
        outputs = None
        alphas = None
        img_names = []
        batches = 0

        for data, target, names in test_loader:

            for val in target:
                targets.append(val)

            img_names += names

            target = target.float()
            if self.hps.use_cuda:
                data, target = data.cuda(), target.cuda()

            data, target = Variable(data, volatile=True), Variable(target)
            output_, outputs_, alphas_ = self.model(data)
            batches += 1

            outputs_ = outputs_.cpu().data.numpy()
            output_ = None if output_ is None else output_.cpu().data.numpy()
            alphas_ = alphas_.cpu().data.numpy()

            memity, outputs_ = self.postprocess(output_, outputs_)

            for val in memity:
                predictions.append(val)

            # Append results overl all batches
            output = output_ if output is None else np.concatenate(output, output_)
            outputs = outputs_ if outputs is None else np.concatenate(outputs, outputs_)
            alphas = alphas_ if alphas is None else np.concatenate(alphas, alphas_)

        pr.image_names = img_names
        pr.predictions = predictions
        pr.targets = targets
        pr.outputs = outputs
        pr.attention_masks = alphas
        return pr


    def predict_memorability(self, images_path):

        # Use the data.Dataset class to simplify preprocessing and batch generation on multicore architectures
        dataset = LaMem2(split=images_path, # Load all images if the split points to a directory otherwise expects
                         transform=self.test_transform)

        batch_size = self.hps.test_batch_size
        if len(dataset.data) < batch_size:
            batch_size = len(dataset.data)
            if settings.VERBOSE:
                print("Reducing batch size from ", self.hps.test_batch_size, "to",batch_size)

        num_workers = settings.NUM_WORKERS
        loader = torch.utils.data.DataLoader(dataset, batch_size=batch_size, shuffle=False, drop_last=False,
                                                  num_workers=num_workers)

        pr = self.predict(loader)
        return pr
