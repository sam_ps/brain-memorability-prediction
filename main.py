from amnet import AMNet
from config import *
import settings

def main():
    hps = get_amnet_config()
    amnet = AMNet()
    amnet.init(hps)
    result = amnet.predict_memorability(hps.eval_images)
    results_dict = result.write_to_dict()
    return results_dict

def get_max_score_image(results_dict):
    max_score_image = max(results_dict, key=results_dict.get)
    return max_score_image

if __name__ == "__main__":
    results = main()
    max_score_image = get_max_score_image(results)

    if settings.VERBOSE:
        print(results)
        print(max_score_image)
