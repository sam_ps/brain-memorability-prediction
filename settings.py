CNN = 'ResNet50FC'
MODEL_WEIGHTS = 'weights_35.pkl'
LSTM_STEPS = 3
VERBOSE = True

EVAL_IMAGES = 'images/low'

BATCH_SIZE = 25
NUM_WORKERS = 8
# Change this if using GPU
GPU = -1
USE_CUDA = False
