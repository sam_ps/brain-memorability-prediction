import os
import numpy as np
import torch
from torch.autograd import Variable
import pkg_resources
import settings

class HParameters:

    def __init__(self):
        self.use_cuda = settings.USE_CUDA
        self.cuda_device = 0

        self.use_attention = True
        self.last_step_prediction = False
        self.front_end_cnn = 'ResNet50FC'

        self.l2_req = 0.00001
        self.mem_loc_w = None
        self.seq_steps = 3
        # alpha map cost weight
        self.gamma = 0.00001
        # memorability-location cost weight
        self.omega = 0
        self.test_batch_size = settings.BATCH_SIZE

        self.torch_version_major, self.torch_version_minor = [int(v) for v in torch.__version__.split('.')[:2]]
        torchvision_version = pkg_resources.get_distribution("torchvision").version
        self.torchvision_version_major, self.torchvision_version_minor = [int(v) for v in torchvision_version.split('.')[:2]]

        return

    @property
    def seq_steps(self):
        return self._seq_steps

    @seq_steps.setter
    def seq_steps(self, value):
        self._seq_steps = value
        if value <= 0:
            return

        mem_loc_w = (0.1 ** (np.arange(0, self._seq_steps)))
        self.mem_loc_w = Variable(torch.from_numpy(np.array([mem_loc_w]))).float()

    def __str__(self):
        vars = [attr for attr in dir(self) if not callable(getattr(self,attr)) and not (attr.startswith("__") or attr.startswith("_"))]

        info_str = ''
        for i, var in enumerate(vars):
            val = getattr(self, var)
            if isinstance(val, Variable):
                val = val.data.cpu().numpy().tolist()[0]
            info_str += '['+str(i)+'] '+var+': '+str(val)+'\n'

        return info_str


def get_amnet_config():

    hps = HParameters()
    hps.model_weights = settings.MODEL_WEIGHTS
    hps.eval_images = settings.EVAL_IMAGES

    # Default configuration
    hps.cuda_device = settings.GPU
    hps.seq_steps = settings.LSTM_STEPS
    hps.use_cuda = hps.cuda_device > -1

    hps.l2_req = 0.000001
    hps.target_mean = 0.754
    hps.target_scale = 2.0
    hps.img_mean = [0.485, 0.456, 0.406]
    hps.img_std = [0.229, 0.224, 0.225]

    return hps
